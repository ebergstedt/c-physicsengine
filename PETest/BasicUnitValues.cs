﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using PE.Infrastructure.Values;
using PE.Units.SiBaseUnitDefinitons;
using PE.Units.SiDerivedUnitDefinitions;

namespace PETest
{
    [TestClass]
    public class BasicUnitValues
    {
        [TestMethod]
        public void Metre_Prefix_Kilo()
        {
            Metre m = new Metre(1);

            double expected = Math.Pow(10, 3);            
            Assert.AreEqual(m.Value*MetricPrefixEnum.Kilo.Value(),
               expected);
        }

        [TestMethod]
        public void Newton()
        {
            Newton n = new Newton(new Kilogram(1), new Metre(5), new Second(10));
            Assert.AreEqual(n.Value, 0.05);
        }

        [TestMethod]
        public void UnitBase_Comparer()
        {
            Kilogram kg1 = new Kilogram(5);
            Kilogram kg2 = new Kilogram(10);
            Kilogram kg3 = new Kilogram(10);

            Assert.IsTrue(kg1 < kg2);
            Assert.IsTrue(kg1 <= kg2);
            Assert.IsTrue(kg2 == kg3);
            Assert.IsTrue(kg2 >= kg1);
            Assert.IsTrue(kg2 > kg1);
        }
    }
}
