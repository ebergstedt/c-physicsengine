﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PE.Conversion;
using PE.Units.SiBaseUnitDefinitons;
using PE.Units.SiDerivedUnitDefinitions;

namespace PETest
{
    [TestClass]
    public class Conversion
    {
        [TestMethod]
        public void ToRadianFromDegrees()
        {
            var d = new Degree(200);
            double expected = (200*Math.PI)/180;
            Radian r = d.ToRadian();

            Debug.WriteLine("Degrees {0}, Expected radians {1}", d.Value, expected);
            Assert.AreEqual(expected, r.Value);
        }

        [TestMethod]
        public void ToDegreesFromRadians()
        {
            var r = new Radian(2*Math.PI);
            double expected = 360;
            Degree d = r.ToDegree();

            Debug.WriteLine("Radians {0}, Expected degrees {1}", r.Value, expected);
            Assert.AreEqual(expected, d.Value);            
        }
    }
}
