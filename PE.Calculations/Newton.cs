﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PE.Units.SiBaseUnitDefinitons;
using PE.Units.SiDerivedUnitDefinitions;

namespace PE.Calculations
{
    public static class NewtonCalculator
    {         
        public static Newton Force(Kilogram kg, Metre m, Second s)
        {
            return new Newton(kg, m, s);
        }
    }
}
