﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PE.Units.SiBaseUnitDefinitons;
using PE.Units.SiDerivedUnitDefinitions;

namespace PE.Conversion
{
    public static class UnitConversion
    {
        public static Radian ToRadian(this Degree degree)
        {
            return new Radian(
                (degree.Value*Math.PI) / 180
            );            
        }

        public static Degree ToDegree(this Radian radian)
        {
            return new Degree(radian.Value * (180 / Math.PI));
        }
    }
}
