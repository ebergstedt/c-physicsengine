﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PE.Infrastructure.Values
{
    /// <summary>
    /// https://en.wikipedia.org/wiki/Degree_(angle)#Conversion_of_some_common_angles
    /// </summary>
    public class AngleConversionEnum
    {
        public static readonly AngleConversionEnum Degrees_0 = new AngleConversionEnum(0, 0, 0);
        public static readonly AngleConversionEnum Degrees_30 = new AngleConversionEnum(1/12, 30, Math.PI / 6);
        public static readonly AngleConversionEnum Degrees_45 = new AngleConversionEnum(1 / 8, 45, Math.PI / 4);
        public static readonly AngleConversionEnum Degrees_60 = new AngleConversionEnum(1 / 6, 60, Math.PI / 3);
        public static readonly AngleConversionEnum Degrees_90 = new AngleConversionEnum(1 / 4, 90, Math.PI / 2);
        public static readonly AngleConversionEnum Degrees_180 = new AngleConversionEnum(1 / 2, 180, Math.PI);
        public static readonly AngleConversionEnum Degrees_270 = new AngleConversionEnum(3 / 4, 270, 3 * Math.PI / 2);
        public static readonly AngleConversionEnum Degrees_360 = new AngleConversionEnum(1, 360, 2 * Math.PI);

        private double turns;
        private double degrees;
        private double radians;

        public AngleConversionEnum(double turns, double degrees, double radians)
        {
            this.turns = turns;
            this.degrees = degrees;
            this.radians = radians;
        }
        
        public double Turns()
        {
            return turns;
        }

        public double Degrees()
        {
            return degrees;
        }

        public double Radians()
        {
            return radians;
        }
    }
}
