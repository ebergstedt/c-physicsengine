﻿namespace PE.Infrastructure.Enums
{
    /// <summary>
    /// https://en.wikipedia.org/wiki/SI_unit#Base_units
    /// </summary>
    public class BaseUnitEnum
    {
        public static readonly BaseUnitEnum Metre = new BaseUnitEnum("metre", "m", "length", "L");
        public static readonly BaseUnitEnum Kilogram = new BaseUnitEnum("kilogram", "kg", "mass", "M");
        public static readonly BaseUnitEnum Second = new BaseUnitEnum("second", "s", "time", "M");
        public static readonly BaseUnitEnum Ampere = new BaseUnitEnum("ampere", "A", "electric current", "I");
        public static readonly BaseUnitEnum Kelvin = new BaseUnitEnum("kelvin", "K", "thermodynamic temperature", "Θ");
        public static readonly BaseUnitEnum Mole = new BaseUnitEnum("mole", "mol", "amount of sumstance", "N");
        public static readonly BaseUnitEnum Candela = new BaseUnitEnum("candela", "cd", "luminous intensity", "J");

        private string name;
        private string quantityName;
        private string symbol;
        private string dimensionSymbol;

        public BaseUnitEnum(string name, string symbol, string quantityName, string dimensionSymbol)
        {
            this.name = name;
            this.quantityName = quantityName;
            this.symbol = symbol;            
            this.dimensionSymbol = dimensionSymbol;
        }

        public string Name()
        {
            return name;
        }

        public string Symbol()
        {
            return symbol;
        }

        public string QuantityName()
        {
            return quantityName;
        }

        public string DimensionSymbol()
        {
            return dimensionSymbol;
        }
    }
}
