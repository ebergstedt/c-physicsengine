﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PE.Infrastructure.Values
{
    /// <summary>
    /// https://en.wikipedia.org/wiki/SI_unit#Prefixes
    /// </summary>
    public class MetricPrefixEnum
    {
        public static readonly MetricPrefixEnum Deca = new MetricPrefixEnum("deca", Math.Pow(10, 1));
        public static readonly MetricPrefixEnum Hecto = new MetricPrefixEnum("hecto", Math.Pow(10, 2));
        public static readonly MetricPrefixEnum Kilo = new MetricPrefixEnum("kilo", Math.Pow(10, 3));
        public static readonly MetricPrefixEnum Mega = new MetricPrefixEnum("mega", Math.Pow(10, 6));
        public static readonly MetricPrefixEnum Giga = new MetricPrefixEnum("giga", Math.Pow(10, 9));
        public static readonly MetricPrefixEnum Tera = new MetricPrefixEnum("tera", Math.Pow(10, 12));
        public static readonly MetricPrefixEnum Peta = new MetricPrefixEnum("peta", Math.Pow(10, 15));
        public static readonly MetricPrefixEnum Exa = new MetricPrefixEnum("exa", Math.Pow(10, 18));
        public static readonly MetricPrefixEnum Zetta = new MetricPrefixEnum("zetta", Math.Pow(10, 21));
        public static readonly MetricPrefixEnum Yotta = new MetricPrefixEnum("yotta", Math.Pow(10, 24));

        public static readonly MetricPrefixEnum Deci = new MetricPrefixEnum("deci", Math.Pow(10, -1));
        public static readonly MetricPrefixEnum Centi = new MetricPrefixEnum("centi", Math.Pow(10, -2));
        public static readonly MetricPrefixEnum Milli = new MetricPrefixEnum("milli", Math.Pow(10, -3));
        public static readonly MetricPrefixEnum Micro = new MetricPrefixEnum("micro", Math.Pow(10, -6));
        public static readonly MetricPrefixEnum Nano = new MetricPrefixEnum("nano", Math.Pow(10, -9));
        public static readonly MetricPrefixEnum Pico = new MetricPrefixEnum("pico", Math.Pow(10, -12));
        public static readonly MetricPrefixEnum Femto = new MetricPrefixEnum("femto", Math.Pow(10, -15));
        public static readonly MetricPrefixEnum Atto = new MetricPrefixEnum("atto", Math.Pow(10, -18));
        public static readonly MetricPrefixEnum Zepto = new MetricPrefixEnum("zepto", Math.Pow(10, -21));
        public static readonly MetricPrefixEnum Yocto = new MetricPrefixEnum("yocto", Math.Pow(10, -24));

        private string name;
        private double value;

        private MetricPrefixEnum(string name, double value)
        {
            this.name = name;
            this.value = value;
        }

        public string Name()
        {
            return name;
        }

        public double Value()
        {
            return value;
        }

        public override string ToString()
        {
            return name;
        }
    }
}
