﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PE.Units.SiBaseUnitDefinitons;

namespace PE.Units.SiDerivedUnitDefinitions
{
    public class Degree : Unit
    {
        public Degree(double value)
        {
            Value = value;
        }
    }
}
