﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PE.Units.SiBaseUnitDefinitons;

namespace PE.Units.SiDerivedUnitDefinitions
{
    /// <summary>
    /// https://en.wikipedia.org/wiki/Radian
    /// </summary>
    public class Radian : Unit
    {
        public Radian(double value)
        {
            Value = value;
        }

        public Radian (Metre m1, Metre m2)
        {
            Value = m1.Value / m2.Value;
        }
    }
}
