﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PE.Units.SiBaseUnitDefinitons;

namespace PE.Units.SiDerivedUnitDefinitions
{
    public class Newton : Unit
    {
        public static Newton G = new Newton(6.67384 * Math.Pow(10, -11));

        public Newton(double value)
        {
            Value = value;
        }

        public Newton(Kilogram kg, Metre m, Second s)
        {
            Value = kg.Value * (m.Value / Math.Pow(s.Value, 2));
        }
    }
}
