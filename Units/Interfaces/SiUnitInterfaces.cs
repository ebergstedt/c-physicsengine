﻿using System.Collections.Generic;
using PE.Infrastructure.Enums;
using PE.Units.SiBaseUnitDefinitons;

namespace PE.Units.Interfaces
{
    public interface IBaseUnit
    {
        BaseUnitEnum GetUnit();
    }
}
