﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PE.Units.SiBaseUnitDefinitons
{
    /// <summary>
    /// https://en.wikipedia.org/wiki/SI_unit
    /// </summary>
    public class Unit
    {
        public double Value { get; set; }

        public static bool operator ==(Unit left, Unit right)
        {
            return left.Value == right.Value;
        }
        public static bool operator !=(Unit left, Unit right)
        {
            return left.Value != right.Value;
        }
        public static bool operator <(Unit left, Unit right)
        {
            return left.Value < right.Value;
        }
        public static bool operator >(Unit left, Unit right)
        {
            return left.Value > right.Value;
        }
        public static bool operator <=(Unit left, Unit right)
        {
            return left.Value <= right.Value;
        }
        public static bool operator >=(Unit left, Unit right)
        {
            return left.Value >= right.Value;
        }
    }
}
