﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PE.Infrastructure.Enums;
using PE.Units.Interfaces;

namespace PE.Units.SiBaseUnitDefinitons
{
    [Serializable]
    public class Mole : Unit, IBaseUnit
    {
        public Mole(double value)
        {
            Value = value;
        }

        public BaseUnitEnum GetUnit()
        {
            return BaseUnitEnum.Mole;
        }
    }
}
